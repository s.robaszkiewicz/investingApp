package com.robaszkiewicz.s.investing.investing.repository;

import com.robaszkiewicz.s.investing.investing.model.ClosedPositions;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClosedPositionsRepository extends CrudRepository<ClosedPositions, Long> {

}
