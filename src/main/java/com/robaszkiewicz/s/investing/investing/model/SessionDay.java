package com.robaszkiewicz.s.investing.investing.model;

import javax.persistence.*;
import java.util.Date;


public class SessionDay {


    private String triname;
    private String date;
    private int openPrice;
    private int highestPrice;
    private int lowestPrice;
    private int closePrice;
    private int volume;

    public SessionDay(long id, String triname, String date, int openPrice, int highestPrice, int lowestPrice, int closePrice, int volume) {

        this.triname = triname;
        this.date = date;
        this.openPrice = openPrice;
        this.highestPrice = highestPrice;
        this.lowestPrice = lowestPrice;
        this.closePrice = closePrice;
        this.volume = volume;
    }

    public String getTriname() {
        return triname;
    }

    public void setTriname(String triname) {
        this.triname = triname;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getOpenPrice() {
        return openPrice;
    }

    public void setOpenPrice(int openPrice) {
        this.openPrice = openPrice;
    }

    public int getHighestPrice() {
        return highestPrice;
    }

    public void setHighestPrice(int highestPrice) {
        this.highestPrice = highestPrice;
    }

    public int getLowestPrice() {
        return lowestPrice;
    }

    public void setLowestPrice(int lowestPrice) {
        this.lowestPrice = lowestPrice;
    }

    public int getClosePrice() {
        return closePrice;
    }

    public void setClosePrice(int closePrice) {
        this.closePrice = closePrice;
    }

    public int getVolume() {
        return volume;
    }

    public void setVolume(int volume) {
        this.volume = volume;
    }
}
