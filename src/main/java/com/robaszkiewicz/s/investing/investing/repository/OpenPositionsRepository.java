package com.robaszkiewicz.s.investing.investing.repository;

import com.robaszkiewicz.s.investing.investing.model.ClosedPositions;
import com.robaszkiewicz.s.investing.investing.model.OpenPositions;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OpenPositionsRepository extends CrudRepository<OpenPositions, Long> {

}
