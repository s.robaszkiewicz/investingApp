package com.robaszkiewicz.s.investing.investing.repository;

import com.robaszkiewicz.s.investing.investing.model.Transaction;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TransactionRepository extends CrudRepository<Transaction, Long> {
}
