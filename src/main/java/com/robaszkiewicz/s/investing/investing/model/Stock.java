package com.robaszkiewicz.s.investing.investing.model;

import sun.util.calendar.LocalGregorianCalendar;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Map;


@Entity
public class Stock {

    @Id
    private String triname;
    private String fullName;
    private String currentPrice;

    //private Map<LocalGregorianCalendar.Date, SessionDay> HistoricalPrices;

    public Stock(String triname, String fullName, String currentPrice, Map<LocalGregorianCalendar.Date, SessionDay> historicalPrices) {
        this.triname = triname;
        this.fullName = fullName;
        this.currentPrice = currentPrice;
        //HistoricalPrices = historicalPrices;
    }

//    public Map<LocalGregorianCalendar.Date, SessionDay> getHistoricalPrices() {
//        return HistoricalPrices;
//    }
//
//    public void setHistoricalPrices(Map<LocalGregorianCalendar.Date, SessionDay> historicalPrices) {
//        HistoricalPrices = historicalPrices;
//    }

    public String getTriname() {
        return triname;
    }

    public void setTriname(String triname) {
        this.triname = triname;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getCurrentPrice() {
        return currentPrice;
    }

    public void setCurrentPrice(String currentPrice) {
        this.currentPrice = currentPrice;
    }
}
