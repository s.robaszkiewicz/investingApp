package com.robaszkiewicz.s.investing.investing.model;

import javax.persistence.*;
import java.util.List;

@Entity
public class OpenPositions {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @ManyToOne
    @JoinColumn
    private Wallet wallet;
    private String triname;
    private double avgBuyPrice;
    private int quantity;
    private String closedOn;

    public OpenPositions() {
    }

    public OpenPositions(Wallet wallet, String triname, double avgBuyPrice, int quantity, String closedOn) {
        this.wallet = wallet;
        this.triname = triname;
        this.avgBuyPrice = avgBuyPrice;
        this.quantity = quantity;
        this.closedOn = closedOn;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Wallet getWallet() {
        return wallet;
    }

    public void setWallet(Wallet wallet) {
        this.wallet = wallet;
    }

    public String getTriname() {
        return triname;
    }

    public void setTriname(String triname) {
        this.triname = triname;
    }

    public double getAvgBuyPrice() {
        return avgBuyPrice;
    }

    public void setAvgBuyPrice(double avgBuyPrice) {
        this.avgBuyPrice = avgBuyPrice;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getClosedOn() {
        return closedOn;
    }

    public void setClosedOn(String closedOn) {
        this.closedOn = closedOn;
    }
}
