package com.robaszkiewicz.s.investing.investing.model;

import javax.persistence.*;
import java.util.List;

@Entity
public class User {
    @Id
    private
    String login;
    private String firstName;
    private String surname;
    private String email;
    private String password;
    @OneToMany(mappedBy = "user")
    private List<Wallet> wallets;

    public User(String login, String firstName, String surname, String email, String password, List<Wallet> wallets) {
        this.login = login;
        this.firstName = firstName;
        this.surname = surname;
        this.email = email;
        this.password = password;
        this.wallets = wallets;
    }

    public User() {
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<Wallet> getWallets() {
        return wallets;
    }

    public void setWallets(List<Wallet> wallets) {
        this.wallets = wallets;
    }
}
