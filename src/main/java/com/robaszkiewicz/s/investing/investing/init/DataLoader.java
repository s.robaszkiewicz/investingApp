package com.robaszkiewicz.s.investing.investing.init;

import com.robaszkiewicz.s.investing.investing.model.ClosedPositions;
import com.robaszkiewicz.s.investing.investing.model.OpenPositions;
import com.robaszkiewicz.s.investing.investing.model.User;
import com.robaszkiewicz.s.investing.investing.model.Wallet;
import com.robaszkiewicz.s.investing.investing.repository.ClosedPositionsRepository;
import com.robaszkiewicz.s.investing.investing.repository.OpenPositionsRepository;
import com.robaszkiewicz.s.investing.investing.repository.UserRepository;
import com.robaszkiewicz.s.investing.investing.repository.WalletRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Component
public class DataLoader {

    @Autowired
    ClosedPositionsRepository closedPositionsRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    WalletRepository walletRepository;
    @Autowired
    OpenPositionsRepository openPositionsRepository;


    private List<Wallet> listaPortfeli = new ArrayList<>();


    @PostConstruct
    public void init() {
        User user = new User("login", "imie", "nazwisko", "s.robaszkiewicz@gmail.com", "admin", listaPortfeli);

        List<OpenPositions> op = new ArrayList<>();
        List<ClosedPositions> cp = new ArrayList<>();




        userRepository.save(user);
        Wallet wallet1 = new Wallet(user, "testowyPortfel", cp, op);
        listaPortfeli.add(wallet1);
        walletRepository.save(wallet1);
        ClosedPositions closedPositions = new ClosedPositions(wallet1,"CDR", 100.0,110,20,"2017-10-10");
        OpenPositions openPositions = new OpenPositions(wallet1,"CDR", 100.0,20,"2017-10-10");
        openPositionsRepository.save(openPositions);
        closedPositionsRepository.save(closedPositions);
        op.add(openPositions);
        cp.add(closedPositions);
    }

}
