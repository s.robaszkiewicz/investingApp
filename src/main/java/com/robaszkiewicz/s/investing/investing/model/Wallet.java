package com.robaszkiewicz.s.investing.investing.model;

import javax.persistence.*;
import java.util.List;

@Entity
public class Wallet {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id")
    private User user;
    private String name;
    @OneToMany(mappedBy = "wallet")
    private
    List<ClosedPositions> closedPositions;
    @OneToMany(mappedBy = "wallet")
    private
    List<OpenPositions> openPositions;

    public Wallet() {
    }

    public Wallet(User user, String name, List<ClosedPositions> closedPositions, List<OpenPositions> openPositions) {
        this.user = user;
        this.name = name;
        this.closedPositions = closedPositions;
        this.openPositions = openPositions;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<ClosedPositions> getClosedPositions() {
        return closedPositions;
    }

    public void setClosedPositions(List<ClosedPositions> closedPositions) {
        this.closedPositions = closedPositions;
    }

    public List<OpenPositions> getOpenPositions() {
        return openPositions;
    }

    public void setOpenPositions(List<OpenPositions> openPositions) {
        this.openPositions = openPositions;
    }
}
