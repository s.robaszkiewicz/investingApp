package com.robaszkiewicz.s.investing.investing.repository;

import com.robaszkiewicz.s.investing.investing.model.Stock;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StockRepository extends CrudRepository<Stock, String>{

}
