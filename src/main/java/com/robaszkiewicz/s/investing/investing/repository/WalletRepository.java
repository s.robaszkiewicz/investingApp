package com.robaszkiewicz.s.investing.investing.repository;

import com.robaszkiewicz.s.investing.investing.model.Wallet;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface WalletRepository extends CrudRepository<Wallet, Long>{
}
